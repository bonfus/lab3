# coding: utf-8
from ase.io import read
from ase.io.trajectory import Trajectory
import numpy as np
import os, warnings
import numpy as np
from scipy.fftpack import fft
from scipy.signal import convolve, gaussian
from scipy.constants import h, eV

import matplotlib.pyplot as plt

def pyvacf(vel, m=None, method=3):
    """Reference implementation for calculating the VACF of velocities in 3d
    array `vel`.
    
    Taken from https://github.com/elcorto/pwtools/ .

    Parameters
    ----------
    vel : 3d array, (nstep, natoms, 3)
        Atomic velocities.
    m : 1d array (natoms,)
        Atomic masses.
    method : int
        | 1 : 3 loops
        | 2 : replace 1 inner loop
        | 3 : replace 2 inner loops

    Returns
    -------
    c : 1d array (nstep,)
        VACF
    """
    natoms = vel.shape[1]
    nstep = vel.shape[0]
    c = np.zeros((nstep,), dtype=float)
    if m is None:
        m = np.ones((natoms,), dtype=float)
    if method == 1:
        # c(t) = <v(t0) v(t0 + t)> / <v(t0)**2> = C(t) / C(0)
        #
        # "displacements" `t'
        for t in range(nstep):
            # time origins t0 == j
            for j in range(nstep-t):
                for i in range(natoms):
                    c[t] += np.dot(vel[j,i,:], vel[j+t,i,:]) * m[i]
    elif method == 2:
        # replace 1 inner loop
        for t in range(nstep):
            for j in range(nstep-t):
                # (natoms, 3) * (natoms, 1) -> (natoms, 3)
                c[t] += (vel[j,...] * vel[j+t,...] * m[:,None]).sum()
    elif method == 3:
        # replace 2 inner loops:
        # (xx, natoms, 3) * (1, natoms, 1) -> (xx, natoms, 3)
        for t in range(nstep):
            c[t] = (vel[:(nstep-t),...] * vel[t:,...]*m[None,:,None]).sum()
    else:
        raise ValueError('unknown method: %s' %method)
    # normalize to unity
    c = c / c[0]
    return c


if __name__ == '__main__':
    # Use same amount of steps
    min_steps = 1e7
    for fname in ('300','400','500','600','700','800','900','1000'):
        tr = Trajectory('md_{}.traj'.format(fname))
        min_steps = min(len(tr),min_steps)
    
    
    for fname in ('300','400','500','600','700','800','900','1000'):
        tr = Trajectory('md_{}.traj'.format(fname))
        dt = 10 * 4.8378e-17 # s
        
        # Collect positions
        poss = np.zeros([min_steps,36,3])
        for i in range(min_steps):
            poss[i] = tr[i].get_positions()
            
        # Compute velocities
        v = np.diff(poss*1e-10,axis=0)/dt
        
        # Compute velocity autocorrelation function
        vacf = pyvacf(v[100:], m=tr[0].get_masses())
        
        # (possibly) Show it
        #plt.plot(vacf)
        #plt.show()
        
        # Fourier transmorm it
        fft_vacf = fft(vacf)
        
        full_faxis = np.fft.fftfreq(fft_vacf.shape[0], dt)
        full_pdos = np.real(fft_vacf)
        
        # Get positive side
        split_idx = len(full_faxis)//2
        faxis = full_faxis[:split_idx]
        pdos = full_pdos[:split_idx]
        
        plt.plot(1000 * faxis * h / eV, pdos, label = '~{} K'.format(int(fname)/2))
        plt.xlabel('Freq. [meV]')
        plt.xlim([0,500])
        plt.legend()
        plt.savefig('{}K.png'.format(fname))
        
        #plt.show()
