# coding: utf-8
from ase.io import read
from ase.io.trajectory import Trajectory
import numpy as np
import os, warnings
import numpy as np
from scipy.fftpack import fft
from scipy.signal import convolve, gaussian
from scipy.constants import h, eV
from scipy.io import savemat


for fname in ('300','400','500','600','700','800','900','1000'):
    tr = Trajectory('md_{}.traj'.format(fname))
    poss = np.zeros([len(tr),36,3])
    for i in range(len(tr)):
        poss[i] = tr[i].get_positions()
    savemat('{}K_pos.mat'.format(fname),{'positions': poss, 'masses': tr[0].get_masses()})

